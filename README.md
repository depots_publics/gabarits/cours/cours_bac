# Gabarit LaTeX pour cours du baccalauréat

Ce dépôt contient le gabarit des notes du cours mec1420 (résistance des matériaux 1) donné aux étudiants de génie mécanique et génie aérospatial.

## Commande de compilation

### sous Linux

./compilation.sh

### sous Windows

pdflatex notes_de_cours.tex
